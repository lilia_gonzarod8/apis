console.log("API de Usuarios");

var listausuariosJSON = require('./listausuarios.json')
var express = require('express') /* variable que hace referencia al componente Express */
var app = express() /* Es como si levantara un servidor */
var bodyparser = require('body-parser')

var app = express()
app.use = (bodyparser.json())

app.get('/usuarios',function(req, res)
{
  res.send('Hola Lista de Usuarios')
})

app.get('/v1/movimientos',function(req, res){
  /* res.send('mov1,mov2,mov3')  */
  res.sendfile('movimientosV1.json')
})

app.get('/v2/movimientos',function(req, res){
  /* res.send('mov1,mov2,mov3') */
  res.sendfile('movimientosV2.json')
})

app.get('/v2/movimientos/:id',function(req, res){
  //console.log(req)
  //console.log(movimientosJSON)
  console.log(req.params.id)
  //res.send("Hemos recibido su peticion de consulta del movimiento No." + req.params.id)
  res.send(movimientosJSON[req.params.id-1])
})

app.post('/v2/movimientos',function(req, res){
    console.log(req)
//    console.log(req.headers['autorization'])
//    if (req.headers['autorization']!=undefined)
//    {
      var nuevo = req.body
      nuevo.id = movimientosJSON.length + 1
      movimientosJSON.push(nuevo)
      res.send("Movimiento dado de alta")
//    }
//    else    {
//      res.send("No esta autorizado")
//    }
  })

app.put('/v2/movimientos/:id',function(req,res){
    //console.log(req)
    var cambios = req.body
    var actual = movimientosJSON[req.params.id-1]
    if (cambios.importe != undefined)
    {
      actual.importe = cambios.importe
    }
    if (cambios.ciudad != undefined)
    {
      actual.ciudad = cambios.ciudad
  }
      res.send("Modificacion Realizada")
})

app.delete('/v2/movimientos/:id',function(req, res){
  var actual = movimientosJSON[req.params.id-1]
  movimientosJSON.push({
    "id": movimientosJSON.length+1,
    "ciudad": actual.ciudad,
    "importe": actual.importe * (-1),
    "concepto": "Negativo del " + req.params.id
  })
  res.send("Movimiento Anulado")
})

app.listen(3000)
console.log("Escuchando en el puerto 300")
