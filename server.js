console.log("Aqui funcionando con Nodemon");

var movimientosJSON = require('./movimientosV2.json')
var usuariosJSON = require ('./Usuarios.json')
var express = require('express') /* variable que hace referencia al componente Express */
var app = express() /* Es como si levantara un servidor */
var bodyparser = require('body-parser')
var requestJson = require('request-json')

var app = express()
app.use = (bodyparser.json())

app.get('/',function(req, res)
{
  res.send('Hola API')
})

app.get('/v1/movimientos',function(req, res){
  /* res.send('mov1,mov2,mov3')  */
  res.sendfile('movimientosV1.json')
})

app.get('/v2/movimientos',function(req, res){
  /* res.send('mov1,mov2,mov3') */
  res.sendfile('movimientosV2.json')
})

app.get('/v2/movimientos/:id',function(req, res){
  //console.log(req)
  //console.log(movimientosJSON)
  console.log(req.params.id)
  //res.send("Hemos recibido su peticion de consulta del movimiento No." + req.params.id)
  res.send(movimientosJSON[req.params.id-1])
})

app.post('/v2/movimientos',function(req, res){
    console.log(req)
//    console.log(req.headers['autorization'])
//    if (req.headers['autorization']!=undefined)
//    {
      var nuevo = req.body
      nuevo.id = movimientosJSON.length + 1
      movimientosJSON.push(nuevo)
      res.send("Movimiento dado de alta")
//    }
//    else    {
//      res.send("No esta autorizado")
//    }
  })

app.put('/v2/movimientos/:id',function(req,res){
    //console.log(req)
    var cambios = req.body
    var actual = movimientosJSON[req.params.id-1]
    if (cambios.importe != undefined)
    {
      actual.importe = cambios.importe
    }
    if (cambios.ciudad != undefined)
    {
      actual.ciudad = cambios.ciudad
  }
      res.send("Modificacion Realizada")
})

app.delete('/v2/movimientos/:id',function(req, res){
  var actual = movimientosJSON[req.params.id-1]
  movimientosJSON.push({
    "id": movimientosJSON.length+1,
    "ciudad": actual.ciudad,
    "importe": actual.importe * (-1),
    "concepto": "Negativo del " + req.params.id
  })
  res.send("Movimiento Anulado")
})

app.get('/usuarios/:email',function(req, res)
{
  var email = req.headers['email']
  var correo = usuariosJSON[req.params.email-1]
  if (correo == email) {
    res.send("Usuario encontrado")
    res.send(usuariosJSON[req.params.id-1])
  }
  else   {
    res.send("Usuario no encontrado")
    correo = usuariosJSON[id+1]
  }
 })

app.post('usuarios/login',function(req, res){
  var email = req.headers['email']
  var password = req.headers['password']
  var resultados = jsonQuery('[email=' + email + ']', {data:usuariosJSON})
  if (resultados.value != null && resultados.value.password ==password){
    usuariosJSON[resultados.value.id-1].estado='y'
    res.send('login OK')
  }
  else {
    res.send('login NOK')
  }
})


app.post('/usuarios/logout/:id',function(req, res){
    var id = req.params.id
    var usuario = usuariosJSON[id-1]
    if (usuario.estado == 'logged') {
      usuario.estado = 'logout'
      res.send('{"logout":"OK"}')
    }
    else {
      res.send('{"logout":"error"}')
    }
})


//Version 3 de la API. Conexion con Mongo a traves de MLab
// Definicion de variables para la version 3

var urlMlabRaiz = 'https://api.mlab.com/api/1/databases/techumxlgr/collections'
var apiKey = "apiKey=kFC4T0jvY88MtuTOTiAIBfEh5WpPVhJZ"
var clienteMlab = requestJson.createClient(urlMlabRaiz + '?' + apiKey)

//Consulta de las colecciones existentes en la BD techumxlgr
app.get('/v3',function(req, res)
{
//vamos a hacer la llamada a Mongo a traves de la API de MLab
  clienteMlab.get('',function(err, resmM, body){
    var coleccionesUsuario = [] // atrapamos la respuesta en un arreglo
    if (!err) {
      for (var i = 0; i < body.length; i++) {
        if (body[i] != "system indexes"){
          coleccionesUsuario.push({"recurso":body[i], "url":"/v3/" + body[i]})
        }
      }
      res.send(coleccionesUsuario)
    }
    else {
      res.send(err)
    }
  })
})

//Consulta de Usuarios: obtiene todos los usuarios existentes en la coleccion
app.get('/v3/usuarios', function(req, res){
  clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?' + apiKey)
  clienteMlab.get('',function(err, resmM, body){
      res.send(body)
  })
})

//Alta de Usuario
app.post('/v3/usuarios', function(req, res){
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
    clienteMlab.post('',req.body,function(err, remM, body){
      res.send(body)
    })
})

//Consulta de Usuarios especìficos
app.get('/v3/usuarios/:id',function(req, res){
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
  clienteMlab.get('?q={"id":'+req.params.id + '}&' + apiKey, function(err, remM, body){
    res.send(body)
  })
})

//Modificacion de un usuario particular
//Cuidado: de manera natural sustituira todo el registro con los datos de entrada
//Para evitar esto, colocar las variables $set y si quieres que modifique todos,
//Colocar la opcion m=true
//app.put('/v3/usuarios/:id', function(req,res){
//  clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?q={"id":}')
//  var cambio ='{"$set":' `JSON.stringify(req.body)`'}'
//})

//Borrado de un usuario por condicion
app.delete('/v3/usuarios/:id', function(req, res){
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
  clienteMlab.delete('?q={"id":'+req.params.id + '}&' + apiKey, function(err, remM, body){
  res.send(body)
  })
})

app.listen(3000)
console.log("Escuchando en el puerto 300")
