var mocha = require('mocha')
var chai = require ('chai')
var chaiHttp = require('chai-http')
var should = chai.should()
//Para indicarle que tiene que ejecutar el archivo server.js
// nos evitamos "levantar" el servidor con $npm start
var server =require('../server')

// para indicarle que queremos que utilice las reglas de https
chai.use(chaiHttp)

//para indicarle que  pruebas queremos hacer
describe('Test de conectividad', () => {
  it('Google funciona', (done) => {
    chai.request('http://www.google.com.mx')
        .get('/')
        .end((err, res) => {
//          console.log(res)
            res.should.have.status(200)
          done()
        })
  })
})

//Test de api de usuarios
describe('Test de API usuarios', () => {
  it('Raiz de que la API funciona', (done) => {
    chai.request('http://localhost:3000')
        .get('/')
        .end((err, res) => {
//          console.log(res)
            res.should.have.status(200)
          done()
        })
  })

  it('Validacion de arreglo', (done) => {
    chai.request('http://localhost:3000')
        .get('/v3')
        .end((err, res) => {
          console.log(res.body)
            res.should.have.status(200)
// Para validar si me esta regresando un arreglo de elementos
            res.body.should.be.a('array')
          done()
        })
    })

    it('Logitud del arreglo. Numero de colecciones', (done) => {
      chai.request('http://localhost:3000')
          .get('/v3')
          .end((err, res) => {
              console.log(res.body)
              res.should.have.status(200)
  // Para validar si me esta regresando un arreglo de elementos
              res.body.should.be.a('array')
              res.body.length.should.be.eql(3)
            done()
          })
      })

      it('Logitud del arreglo. Numero y datos de la coleccion', (done) => {
        chai.request('http://localhost:3000')
            .get('/v3')
            .end((err, res) => {
//                console.send(res.body)
                res.should.have.status(200)
// Para validar si me esta regresando un arreglo de elementos
                res.body.should.be.a('array')
                res.body.length.should.be.eql(3)
                for (var i = 0; i < res.body.length; i++) {
                  res.body[i].should.have.property('recurso')
                  res.body[i].should.have.property('url')
                }
              done()
            })
        })
})


// Validacion de la API de Movimientos
describe('Test de API movimientos', () => {
        it('Validacion de Movimientos', (done) => {
          chai.request('http://localhost:3000')
              .get('/v2/movimientos')
              .end((err, res) => {
                console.log(res.body)
                  res.should.have.status(200)
      // Para validar si me esta regresando un arreglo de elementos
                  res.body.should.be.a('array')
                done()
              })
          })

        it('Validacion de Movimientos particulares', (done) => {
            chai.request('http://localhost:3000')
                .get('/v2/movimientos/25')
                .end((err, res) => {
                  console.log(res.body)
                    res.should.have.status(200)
// Verificar porque no me esta validando la respuesta como un arreglo cuando si lo es
//                    res.body.should.be.a('array')
                  done()
                })
            })

})
